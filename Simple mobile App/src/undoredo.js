(() => {
    class Undoredojs {
        constructor(cooldownNumber) {
            if (!cooldownNumber || isNaN(cooldownNumber) || cooldownNumber <= 0) this.cooldownNumber = 1
            else this.cooldownNumber = cooldownNumber
            this.stack = ['']
            this.currentNumber = 0
            this.currentCooldownNumber = 0
        }
        record(data, force) {
            if (this.currentNumber === this.stack.length - 1) { 
                if ((this.currentCooldownNumber >= this.cooldownNumber || this.currentCooldownNumber === 0) && force !== true) {
                    this.stack.push(data)
                    this.currentNumber++
                    this.currentCooldownNumber = 1
                } else if (this.currentCooldownNumber < this.cooldownNumber && force !== true) {
                    this.current(data)
                    this.currentCooldownNumber++
                } else if (force === true) {
                    this.stack.push(data)
                    this.currentNumber++
                    this.currentCooldownNumber = this.cooldownNumber
                }
            } else if (this.currentNumber < this.stack.length - 1) {
                if (force !== true) {
                    this.stack.length = this.currentNumber + 1
                    this.stack.push(data)
                    this.currentNumber++
                    this.currentCooldownNumber = 1
                } else if (force === true) {
                    this.stack.length = this.currentNumber + 1
                    this.stack.push(data)
                    this.currentNumber++
                    this.currentCooldownNumber = this.cooldownNumber
                }
            }
        }
        undo(readOnly) {
            if (this.currentNumber > 0) {
                if (readOnly !== true) {
                    this.currentNumber--
                    return this.stack[this.currentNumber]
                } else {
                    return this.stack[this.currentNumber - 1]
                }
            }
        }
        redo(readOnly) {
            if (this.currentNumber < this.stack.length - 1) {
                if (readOnly !== true) {
                    this.currentNumber++
                    return this.stack[this.currentNumber]
                } else {
                    return this.stack[this.currentNumber + 1]
                }
            }
        }
        current(data) {
            if (data) this.stack[this.currentNumber] = data
            return this.stack[this.currentNumber]
        }
    }
    if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
        module.exports = Undoredojs
    } 
    if (typeof window === 'object') {
        window.Undoredojs = Undoredojs
    }
})()
